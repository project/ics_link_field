<?php

namespace Drupal\ics_link_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'ics_link_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "ics_link_field_formatter",
 *   label = @Translation("ICS Link Field Formatter"),
 *   field_types = {
 *     "ics_link_field_type"
 *   }
 * )
 */
class IcsLinkFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
        'link_label' => 'Add to Calendar',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return [
        'link_label' => [
          '#title' => $this->t('Link Label'),
          '#type' => 'textfield',
          '#default_value' => $this->getSetting('link_label'),
        ],
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Link label: %link_label', [
      '%link_label' => $this->getSetting('link_label'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items as $delta => $item) {
      $parent_entity_id = $items->getParent()?->getEntity()->id();
      if ($parent_entity_id === NULL) {
        continue;
      }

      $elements[$delta] = [
        '#title' => $this->t($this->getSetting('link_label')),
        '#type' => 'link',
        '#url' => Url::fromRoute('ics_link_field.download', [
          'entity_type' => $this->fieldDefinition->get('entity_type'),
          'field_name' => $this->fieldDefinition->get('field_name'),
          'entity' => $parent_entity_id,
        ]),
      ];
    }

    return $elements;
  }

}
