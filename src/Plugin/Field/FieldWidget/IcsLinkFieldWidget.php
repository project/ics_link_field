<?php

namespace Drupal\ics_link_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ics_link_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "ics_link_field_widget",
 *   module = "ics_link_field",
 *   label = @Translation("ICS Link Widget"),
 *   field_types = {
 *     "ics_link_field_type"
 *   }
 * )
 */
class IcsLinkFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t(
      'This field should be in the visible area if it was added to the ' .
      'field list after content has been created for this bundle. ' .
      'You can save all those contents to apply the computed value and then ' .
      'safely move this field to the disabled area.'
    );
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state,
  ): array {
    $element['value'] = $element + [
        '#title' => $this->fieldDefinition->getName(),
        '#type' => 'hidden',
        '#default_value' => '',
        '#disabled' => TRUE,
        '#description' => $this->t('Normally this field should not be shown!'),
      ];
    return $element;
  }

}
