<?php

namespace Drupal\ics_link_field\Plugin\Field\FieldType;

use Drupal;
use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\token\TokenEntityMapperInterface;

/**
 * Plugin implementation of the 'ics_link_field_type' field type.
 *
 * @FieldType(
 *   id = "ics_link_field_type",
 *   module = "ics_link_field",
 *   label = @Translation("ICS Link"),
 *   description = @Translation("Creates the URL & stores configuration for
 *   generating the ics file"), category = @Translation("Computed"),
 *   default_widget = "ics_link_field_widget", default_formatter =
 *   "ics_link_field_formatter"
 * )
 */
final class IcsLinkFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Text value'))
      ->setComputed(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    // @TODO: Remove dummy schema & return empty array when drupal supports fields without storage
    //
    // According to Drupal\Core\Field\FieldItemInterface::schema
    // "Computed fields having no schema should return an empty array."
    // This however doesn't work...
    //
    // See also:
    // https://www.drupal.org/project/drupal/issues/2986836
    // https://www.drupal.org/project/drupal/issues/2932273
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'length' => 1,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
        'title' => '',
        'location' => '',
        'description' => '',
        'start_date' => '',
        'end_date' => '',
        'start_time' => '',
        'end_time' => '',
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $settings_form = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    $settings_form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Summary'),
      '#default_value' => $settings['title'] ?? 'Event',
      '#required' => TRUE,
      '#description' => t("This will be used as event's summary or title."),
    ];

    $settings_form['location'] = [
      '#type' => 'textfield',
      '#title' => t('Location'),
      '#default_value' => $settings['location'] ?? NULL,
      '#required' => FALSE,
    ];

    $settings_form['description'] = [
      '#type' => 'textarea',
      '#rows' => 3,
      '#cols' => 70,
      '#resizable' => "vertical",
      '#title' => t('Description'),
      '#default_value' => $settings['description'] ?? NULL,
      '#required' => FALSE,
    ];

    $settings_form['start_date'] = [
      '#type' => 'textfield',
      '#title' => t('Start Date'),
      '#default_value' => $settings['start_date'] ?? NULL,
      '#required' => TRUE,
      '#description' => t("Event's start date. Input format must be HTML Date (Y-m-d)."),
    ];

    $settings_form['end_date'] = [
      '#type' => 'textfield',
      '#title' => t('End Date'),
      '#default_value' => $settings['end_date'] ?? NULL,
      '#description' => t("Event's end date. Input format must be HTML Date (Y-m-d). Skip it for single day events"),
      '#required' => FALSE,
    ];

    $settings_form['start_time'] = [
      '#type' => 'textfield',
      '#title' => t('Start Time'),
      '#default_value' => $settings['start_time'] ?? NULL,
      '#description' => t("Event's start time. Input format must be HTML Time (H:i:s). Skip it for all day events"),
      '#required' => FALSE,
    ];

    $settings_form['end_time'] = [
      '#type' => 'textfield',
      '#title' => t('End Time'),
      '#default_value' => $settings['end_time'] ?? NULL,
      '#description' => t("Event's start time. Input format must be HTML Time (H:i:s). Skip it for all day events"),
      '#required' => FALSE,
    ];

    $entity_type = $this->getFieldDefinition()->getTargetEntityTypeId();

    // @TODO: Make it a dependency once this is committed https://www.drupal.org/node/2053415
    /** @var TokenEntityMapperInterface $mapper */
    $mapper = Drupal::service('token.entity_mapper');
    $token_type = $mapper->getTokenTypeForEntityType($entity_type);

    // @TODO: allow more token types
    $settings_form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$token_type],
      '#global_types' => FALSE,
    ];

    return $settings_form;
  }

}
