<?php

namespace Drupal\ics_link_field\Controller;

use DateTime;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Utility\Token;
use Drupal\field\Entity\FieldConfig;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ServiceController.
 *
 * @package Drupal\ics_link_field\Controller
 */
class ServiceController extends ControllerBase {

  public function __construct(
    protected RequestStack $request,
    protected Token $token,
    protected TransliterationInterface $transliteration,
  ) {}

  /**
   * Create dependency injection.
   */
  public static function create(ContainerInterface $container): ServiceController|static {
    return new static(
      $container->get('request_stack'),
      $container->get('token'),
      $container->get('transliteration')
    );
  }

  /**
   * ICS file download response.
   *
   * @param string $field_name
   *   The field_name which stores ics data.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity this ics will be generated from.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An Response object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function icsDownload(string $field_name, EntityInterface $entity): Response {
    $entity_type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();

    $field_config_name = implode(".", [$entity_type, $bundle, $field_name]);

    /** @var FieldConfig|null $field_config */
    $field_config = $this->entityTypeManager()
      ->getStorage('field_config')
      ->load($field_config_name);

    $field_settings = $field_config?->getSettings() ?? [];

    // @TODO: allow more token types
    $data = [$entity_type => $entity];

    foreach ($field_settings as &$setting_value) {
      $setting_value = $this->token->replace($setting_value, $data, ['clear' => TRUE]);
    }
    unset($setting_value);

    // @TODO: validate inputs (HTML Date for "date" fields / HTML Time for "time" fields)
    if (empty($field_settings['start_date'])) {
      throw new NotFoundHttpException();
    }

    if (empty($field_settings['end_date'])) {
      $field_settings['end_date'] = $field_settings['start_date'];
    }

    $start_date = new DateTime($field_settings['start_date']);
    $end_date = new DateTime($field_settings['end_date']);

    $host = $this->request->getCurrentRequest()?->getHost();
    if ($host === NULL) {
      throw new NotFoundHttpException();
    }
    $vCalendar = new Calendar($host);

    if (empty($field_settings['start_time']) || empty($field_settings['end_time'])) {
      $event = $this->createEvent($field_settings, $start_date, $end_date, TRUE);
      $vCalendar->addComponent($event);
    }
    else {
      // Create multiple events, one for each day.
      // Even if it's a single day event, at least one event will be created.
      $end_date->modify("+1 day");
      do {
        $daily_start = new DateTime($start_date->format('Y-m-d') . " " . $field_settings['start_time']);
        $daily_end = new DateTime($start_date->format('Y-m-d') . " " . $field_settings['end_time']);

        $event = $this->createEvent($field_settings, $daily_start, $daily_end);
        $vCalendar->addComponent($event);

        $start_date->modify("+1 day");
      }
      while ($start_date < $end_date);
    }

    $langcode = $this->languageManager()->getCurrentLanguage()->getId();
    $filename = $this->transliteration->transliterate(
        'event-' . strtolower(Html::cleanCssIdentifier($field_settings['title'])),
        $langcode
      ) . '.ics';
    $content = $vCalendar->render();

    $response = new Response($content);

    // Create the disposition of the file.
    $disposition = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      $filename
    );

    // Set the content disposition.
    $response->headers->set('Content-Disposition', $disposition);
    $response->headers->set('Content-Type', ['text/calendar', 'charset=utf-8']);

    // @TODO: add caching per entity
    // Dispatch request.
    return $response;
  }

  /**
   * Generates an event object for the calendar.
   *
   * @param array $field_settings
   * @param \DateTime $dtStart
   * @param \DateTime $dtEnd
   * @param bool $noTime
   *
   * @return \Eluceo\iCal\Component\Event
   *   The generated Event.
   */
  protected function createEvent(array $field_settings, DateTime $dtStart, DateTime $dtEnd, bool $noTime = FALSE): Event {
    $event = new Event();
    $event->setSummary($field_settings['title']);
    $event->setDescription($field_settings['description']);
    $event->setLocation($field_settings['location']);
    $event->setNoTime($noTime);
    $event->setDtStart($dtStart);
    $event->setDtEnd($dtEnd);
    return $event;
  }

}
